"""Distance module for calculating distance between data input and dataset."""
import numpy as np


def calculate_manhattan(iris_data: np.ndarray, input_data: np.ndarray):
    """Calculate the distance between 2 vectors using manhattan distance.

    Args:
        dataset (np.ndarray): Iris dataset
        input_data (np.ndarray): 1x4 matrix data input

    Returns:
        string: Return prediction
    """
    x_data, y_label, _, target_names = iris_data

    distance = np.sqrt(np.sum(np.abs(x_data - input_data), axis=1))
    distance_index = np.argsort(distance)
    y_pred = target_names[y_label[distance_index[0]]]

    return y_pred


if __name__ == "__main__":
    dataset = [
        np.array([[4.9, 3.0, 1.4, 0.2], [4.9, 3.0, 1.4, 0.9]]),
        [0, 0],
        ["sepal_length", "sepal_width", "petal_length", "petal_width"],
        ["setosa", "versicolor", "virginica"],
    ]
    sample_data = np.array([[4.9, 3.0, 1.4, 0.2]])
    print(calculate_manhattan(dataset, sample_data))
    #test