"""Load iris dataset from scikit-learn."""
from sklearn import datasets


def get_iris_data():
    """Load iris dataset.

    Returns:
        set: consists of X, y, feature names, and target_names
    """
    iris = datasets.load_iris()
    x_data = iris.data
    y_label = iris.target
    features_names = ["sepal_length", "sepal_width", "petal_length", "petal_width"]
    target_names = iris.target_names

    return x_data, y_label, features_names, target_names


if __name__ == "__main__":
    x_data, y_label, features, target_names = get_iris_data()

    print("X", x_data)
    print("y", y_label)
    print("features", features)
    print("target_names", target_names)