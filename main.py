"""Iris Web API Service."""
from fastapi import FastAPI
from pydantic import BaseModel
import numpy as np
from src import distance, iris

dataset = iris.get_iris_data()

app = FastAPI()


class Item(BaseModel):
    """Input class for predict endpoint.

    Args:
        BaseModel (BaseModle): Inherited from pydantic
    """

    sepal_length: float
    sepal_width: float
    petal_length: float
    petal_width: float


@app.get("/")
def homepage():
    """Homepage for the web.

    Returns:
        str: Homepage
    """
    return "Homepage Iris Flower - tags 0.0.2"


@app.post("/predict/")
async def predict(item: Item):
    """Predict function for inference.

    Args:
        item (Item): dictionary of sepal dan petal data

    Returns:
        str: predict the target
    """
    sepal_length = item.sepal_length
    sepal_width = item.sepal_width
    petal_length = item.petal_length
    petal_width = item.petal_width

    data_input = np.array([[sepal_length, sepal_width, petal_length, petal_width]])

    result = distance.calculate_manhattan(dataset, data_input)
    return result